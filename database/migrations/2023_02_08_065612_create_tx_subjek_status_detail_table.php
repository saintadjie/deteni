<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTxSubjekStatusDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tx_subjek_status_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreign('id_subjek')->references('id')->on('tx_subjek')->onDelete('cascade');
            $table->string('id_status');
            $table->text('keterangan')->nullable();
            $table->text('deskripsi')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tx_subjek_status_detail');
    }
}
