<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmLokasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_lokasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_lokasi');
            $table->string('alamat');
            $table->foreign('id_kota')->references('id')->on('tm_kota')->onDelete('cascade');
            $table->foreign('id_satker')->references('id')->on('tm_satker')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_lokasi');
    }
}
