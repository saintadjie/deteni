@extends('layouts.in')

@section('content')
<div class="demo5 mt-30 mb-25">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xxl-12 mb-25">
                <div class="card banner-feature--18 new d-flex bg-white">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card-body px-25">
                                    <h1 class="banner-feature__heading color-dark">Hey {{Auth::User()->nama}}! Welcome to the Dashboard
                                    </h1>
                                    <p class="banner-feature__para color-dark">There are many variations of passages of
                                        Lorem Ipsum available,<br>
                                        ut the majority have suffered passages of Lorem Ipsum available alteration in
                                        some form
                                    </p>
                                    <div class="d-flex justify-content-sm-start justify-content-center">
                                        <button class="banner-feature__btn btn btn-primary color-white btn-md radius-xs fs-15" type="button">Learn More</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="banner-feature__shape">
                                    <img src="{{url('in/img/danial.png')}}" alt="img">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
