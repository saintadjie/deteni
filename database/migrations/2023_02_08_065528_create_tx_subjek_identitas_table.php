<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTxSubjekIdentitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tx_subjek_identitas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreign('id_subjek')->references('id')->on('tx_subjek')->onDelete('cascade');
            $table->string('jenis_identitas');
            $table->string('nomor_identitas');
            $table->string('negara_penerbit');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tx_subjek_identitas');
    }
}
