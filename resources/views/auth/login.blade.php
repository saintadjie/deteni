@extends('layouts.out')

@section('content')
<main class="main-content">
    <div class="admin">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xxl-3 col-xl-4 col-md-6 col-sm-8">
                    <div class="edit-profile">
                        <div class="edit-profile__logos">
                            <a href="#">
                                <img class="dark" src="{{url('out/imigrasi.png')}}" alt="DETENI">
                                <img class="light" src="{{url('out/imigrasi.png')}}" alt="DETENI">
                            </a>
                        </div>

                        <div class="card border-0">
                            <div class="card-header">
                                <div class="edit-profile__title">
                                    <h6>Selamat Datang</h6>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="edit-profile__body">
                                    <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="" id="stripe-login">
                                        @csrf
                                        <div class="row">
                                            <div class="input-field col s12">
                                                @if ($errors->has('nip') || $errors->has('password'))
                                                <div class="alert alert-danger">
                                                    <center>
                                                        <strong>{{ $errors->first('nip') ?: $errors->first('password')}}</strong>
                                                    </center>
                                                </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group mb-25">
                                            <label for="nip">NIP</label>
                                            <input id="login" type="text" class="form-control{{ $errors->has('nip') ? ' is-invalid' : '' }}" name="login" value="{{ old('nip') }}"  tabindex="1" required autofocus>
                                            <div class="invalid-feedback">
                                                Harap isi NIP anda
                                            </div>
                                        </div>

                                        <div class="form-group mb-15">
                                            <label for="password-field">Password</label>
                                                <div class="position-relative">
                                                <input id="password-field" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                                <div class="uil uil-eye-slash text-lighten fs-15 field-icon toggle-password2">
                                                </div>
                                                <div class="invalid-feedback">
                                                    Harap isi Password anda
                                                </div>
                                            </div>
                                        </div>

                                        <div class="admin__button-group button-group d-flex pt-1 justify-content-md-start justify-content-center">
                                            <button type="submit"  class="btn btn-primary btn-default w-100 btn-squared text-capitalize lh-normal px-50 signIn-createBtn ">
                                                LOGIN
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="admin-topbar">
                                <p class="mb-0">
                                    Copyright &copy; 2022 Design For 
                                    <a href="https://imigrasi.go.id" class="color-primary">
                                        IMIGRASI
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="overlayer">
    <div class="loader-overlay">
        <div class="dm-spin-dots spin-lg">
            <span class="spin-dot badge-dot dot-primary"></span>
            <span class="spin-dot badge-dot dot-primary"></span>
            <span class="spin-dot badge-dot dot-primary"></span>
            <span class="spin-dot badge-dot dot-primary"></span>
        </div>
    </div>
</div>

<div class="enable-dark-mode dark-trigger">
    <ul>
        <li>
            <a href="#">
                <i class="uil uil-moon"></i>
            </a>
        </li>
    </ul>
</div>


@endsection
