<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>IMIGRASI - DETENI</title>

        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">

        <!-- inject:css-->

        <link rel="stylesheet" href="{{url('out/css/plugin.min.css')}}">

        <link rel="stylesheet" href="{{url('out/style.css')}}">

        <!-- endinject -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{url('out/imigrasi.png')}}">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.0/css/line.css">
    </head>
    <body>
        @yield('content')
        <!-- inject:js-->

        <script src="{{url('out/js/plugins.min.js')}}"></script>
        <script src="{{url('out/js/script.min.js')}}"></script>


       <!-- endinject-->
    </body>
</html>