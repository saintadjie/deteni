<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/home';
    protected $nip;
    /**

     * Create a new controller instance.

     *

     * @return void

     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->nip = $this->findnip();
    }

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function findnip()
    {
        $login = request()->input('login');
 
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'nip' : 'nip';
 
        request()->merge([$fieldType => $login]);
 
        return $fieldType;
    }

     protected function credentials(\Illuminate\Http\Request $request)
    {
        //return $request->only($this->username(), 'password');
        return ['nip' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
    }
 
    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->nip;
    }

    public function logout(Request $request) {
      Auth::logout();
      return redirect('/login');
    }
}