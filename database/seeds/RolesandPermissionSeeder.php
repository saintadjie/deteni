<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesandPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions    
        // Master Data
        Permission::updateOrCreate(['name' => 'Melihat daftar pengguna']);
        Permission::updateOrCreate(['name' => 'Menambah data pengguna']);
        Permission::updateOrCreate(['name' => 'Mengubah data pengguna']);
        Permission::updateOrCreate(['name' => 'Mengubah data pengguna lain']);
        Permission::updateOrCreate(['name' => 'Menghapus data pengguna']);
        Permission::updateOrCreate(['name' => 'Mengembalikan data pengguna terhapus']);

        Permission::updateOrCreate(['name' => 'Melihat daftar dbdpripassword']);
        Permission::updateOrCreate(['name' => 'Menambah data dbdpripassword']);
        Permission::updateOrCreate(['name' => 'Menghapus data dbdpripassword']);
        Permission::updateOrCreate(['name' => 'Mengubah data dbdpripassword']);

        Permission::updateOrCreate(['name' => 'Melihat daftar logs']);

        // create roles and assign existing permissions
        
        $role = Role::updateOrCreate(['name' => 'SUPERADMIN']);
        $role->givePermissionTo([   
                                    'Melihat daftar pengguna',
                                    'Menambah data pengguna',
                                    'Mengubah data pengguna',
                                    'Mengubah data pengguna lain',
                                    'Menghapus data pengguna',
                                    'Mengembalikan data pengguna terhapus',
                                    'Melihat daftar dbdpripassword', 
                                    'Menambah data dbdpripassword', 
                                    'Menghapus data dbdpripassword',
                                    'Mengubah data dbdpripassword',
                                    'Melihat daftar logs',
                                ]);

        $role = Role::updateOrCreate(['name' => 'PENGGUNA']);
        $role->givePermissionTo([   
                                    'Melihat daftar pengguna', 
                                    'Menambah data pengguna', 
                                    'Mengubah data pengguna',
                                    'Mengubah data pengguna lain',
                                    'Menghapus data pengguna',
                                    'Mengembalikan data pengguna terhapus',
                                ]);
    }
}
