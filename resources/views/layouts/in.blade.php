<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IMIGRASI - DETENI</title>
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@400;500;600;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{url('in/css/plugin.min.css')}}">
    <link rel="stylesheet" href="{{url('in/style.css')}}">

    <link rel="icon" type="image/png" sizes="16x16" href="{{url('out/imigrasi.png')}}">

    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.0/css/line.css">
</head>
<body class="layout-light side-menu">
    <div class="mobile-author-actions"></div>
    <header class="header-top">
        <nav class="navbar navbar-light">
            <div class="navbar-left">
                <div class="logo-area">
                    <a class="navbar-brand" href="index.html">
                        <img class="dark" src="{{url('in/img/logo-dark.png')}}" alt="logo">
                        <img class="light" src="{{url('in/img/logo-white.png')}}" alt="logo">
                    </a>
                    <a href="#" class="sidebar-toggle">
                        <img class="svg" src="{{url('in/img/svg/align-center-alt.svg')}}" alt="img">
                    </a>
                </div>
            </div>

            <div class="navbar-right">
                <ul class="navbar-right__menu">
                    <li class="nav-author">
                        <div class="dropdown-custom">
                            <a href="javascript:;" class="nav-item-toggle"><img src="{{url('in/img/author-nav.jpg')}}" alt="" class="rounded-circle">
                                <span class="nav-item__title">Danial<i class="las la-angle-down nav-item__arrow"></i></span>
                            </a>
                            <div class="dropdown-parent-wrapper">
                                <div class="dropdown-wrapper">
                                    <div class="nav-author__info">
                                        <div class="author-img">
                                            <img src="{{url('in/img/author-nav.jpg')}}" alt="" class="rounded-circle">
                                        </div>
                                        <div>
                                            <h6>{{Auth::User()->nama}}</h6>
                                            <span>♥</span>
                                        </div>
                                    </div>
                                    <div class="nav-author__options">
                                        <ul>
                                            <li>
                                                <a href="">
                                                <i class="uil uil-user"></i> Profile</a>
                                            </li>
                                            <li>
                                                <a href="">
                                                    <i class="uil uil-setting"></i>
                                                Settings</a>
                                            </li>
                                            <li>
                                                <a href="">
                                                <i class="uil uil-users-alt"></i> Activity</a>
                                            </li>
                                        </ul>
                                        <a href="{{url('/logout')}}" class="nav-author__signout">
                                        <i class="uil uil-sign-out-alt"></i> Sign Out</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="navbar-right__mobileAction d-md-none">
                    <a href="#" class="btn-author-action">
                    <img class="svg" src="{{url('in/img/svg/more-vertical.svg')}}" alt="more-vertical"></a>
                </div>
            </div>
        </nav>
    </header>
    <main class="main-content">
        <div class="sidebar-wrapper">
            <div class="sidebar sidebar-collapse" id="sidebar">
                <div class="sidebar__menu-group">
                    <ul class="sidebar_nav">
                        <li>
                            <a href="{{url('/home')}}" class="">
                                <span class="nav-icon uil uil-create-dashboard"></span>
                                <span class="menu-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="menu-title mt-30">
                            <span>Applications</span>
                        </li>
                        <li class="has-child">
                            <a href="#" class="">
                                <span class="nav-icon uil uil-envelope"></span>
                                <span class="menu-text">Administrator</span>
                                <span class="toggle-icon"></span>
                            </a>
                            <ul>
                                <li class="">
                                    <a href="inbox.html">Inbox</a>
                                </li>
                                <li class="">
                                    <a href="read-email.html">Read
                                    Email</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-child">
                            <a href="#" class="">
                                <span class="nav-icon uil uil-bag"></span>
                                <span class="menu-text text-initial">Deteni</span>
                                <span class="toggle-icon"></span>
                            </a>
                            <ul>
                                <li class="">
                                    <a href="products.html">Products</a>
                                </li>
                                <li class="">
                                    <a href="product-details.html">Product
                                    Details</a>
                                </li>
                                <li class="">
                                    <a href="add-product.html">Product
                                    Add</a>
                                </li>
                                <li class="">
                                    <a href="add-product.html">Product
                                    Edit</a>
                                </li>
                                <li class="">
                                    <a href="cart.html">Cart</a>
                                </li>
                                <li class="">
                                    <a href="order.html">Orders</a>
                                </li>
                                <li class="">
                                    <a href="sellers.html">Sellers</a>
                                </li>
                                <li class="">
                                    <a href="invoice.html">Invoices</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-child">
                            <a href="#" class="">
                                <span class="nav-icon uil uil-folder"></span>
                                <span class="menu-text">Pengungsi</span>
                                <span class="toggle-icon"></span>
                            </a>
                            <ul>
                                <li class="">
                                    <a href="projects.html">Project</a>
                                </li>
                                <li class="">
                                    <a href="application-ui.html">Project
                                    Details</a>
                                </li>
                                <li class="">
                                    <a href="create.html">Create
                                    Project</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-child">
                            <a href="#" class="">
                                <span class="nav-icon uil uil-bag"></span>
                                <span class="menu-text text-initial">Laporan</span>
                                <span class="toggle-icon"></span>
                            </a>
                            <ul>
                                <li class="">
                                    <a href="products.html">Products</a>
                                </li>
                                <li class="">
                                    <a href="product-details.html">Product
                                    Details</a>
                                </li>
                                <li class="">
                                    <a href="add-product.html">Product
                                    Add</a>
                                </li>
                                <li class="">
                                    <a href="add-product.html">Product
                                    Edit</a>
                                </li>
                                <li class="">
                                    <a href="cart.html">Cart</a>
                                </li>
                                <li class="">
                                    <a href="order.html">Orders</a>
                                </li>
                                <li class="">
                                    <a href="sellers.html">Sellers</a>
                                </li>
                                <li class="">
                                    <a href="invoice.html">Invoices</a>
                                </li>
                            </ul>
                        </li>

                        <li class="menu-title mt-30">
                            <span>Pages</span>
                        </li>
                        <li class="">
                            <a href="{{url('/logout')}}">
                                <span class="nav-icon uil uil-sign-out-alt"></span>
                                <span class="menu-text">Keluar</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="contents">
            @yield('content')
        </div>
        <footer class="footer-wrapper">
            <div class="footer-wrapper__inside">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="footer-copyright">
                                <p><span>© 2023</span><a href="#">IMIGRASI</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer-menu text-end">
                                <ul>
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Team</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </main>
    <div id="overlayer">
        <div class="loader-overlay">
            <div class="dm-spin-dots spin-lg">
                <span class="spin-dot badge-dot dot-primary"></span>
                <span class="spin-dot badge-dot dot-primary"></span>
                <span class="spin-dot badge-dot dot-primary"></span>
                <span class="spin-dot badge-dot dot-primary"></span>
            </div>
        </div>
    </div>
    <div class="overlay-dark-sidebar"></div>
    <div class="customizer-overlay"></div>

    <script src="{{url('out/js/plugins.min.js')}}"></script>
    <script src="{{url('out/js/script.min.js')}}"></script>

</body>
</html>